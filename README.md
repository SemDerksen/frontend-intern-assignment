# Dept Front-end Intern Assignment
Visual Developer

## Description
A front-end assignment given by Dept Agency in order to test technical knowledge for the Dept internship. It consists of a single webpage, made responsive for various resolutions (mobile/tablet/desktop).

## Usage
This project was fully coded through native HTML, CSS, and JS. Simply run index.html to open the webpage locally within your browser.
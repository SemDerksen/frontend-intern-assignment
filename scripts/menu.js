const openMenuButton = document.querySelector(".hamburger");
const menuOverlay = document.querySelector(".menu-overlay");
const closeMenuButton = document.querySelector(".close-menu-button");
const menuLinks = document.querySelector(".menu-links").children;
let timerData = [];

openMenuButton.addEventListener("click", () => {
    menuOverlay.style.display = "flex";
    menuOverlay.classList.add("menu-fade-in");
    document.body.style.position = "fixed";

    for (let i = 0 ; i < menuLinks.length; i++) {
        timerData.push(setTimeout(function () {
            menuLinks[i].classList.add("item-fade-in");
        }, i * 40))
    }
});

closeMenuButton.addEventListener("click", () => {
    menuOverlay.classList.remove("menu-fade-in");
    menuOverlay.style.display = "";
    document.body.style.position = "relative";
    for (let i = 0 ; i < menuLinks.length; i++) {
        menuLinks[i].classList.remove("item-fade-in"
    )}
    timerData.forEach(function(timer) {
        clearTimeout(timer);
    });
});



